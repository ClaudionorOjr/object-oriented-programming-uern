package br.uern.di.poo.unidade1.aula18;

public class Assalariado extends Funcionario{

    public Assalariado(String nome, int horas, double salario){
        super(nome);
        setHoras(horas);
        setSalario(salario);
    }

}
